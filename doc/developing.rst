Developing with nose
====================

Get the code
------------

.. code-block:: none

   svn co http://python-nose.googlecode.com/svn/trunk/ nose

Read
----
   
.. toctree ::
   :maxdepth: 2

   plugins
   api